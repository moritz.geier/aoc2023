// std
#include <algorithm>
#include <cctype>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

///////////////////////////////////////////////////////////////////////////
namespace
{
    ///////////////////////////////////////////////////////////////////////////
    struct Reveal
    {
        uint8_t red;
        uint8_t green;
        uint8_t blue;

        Reveal()
        : red{0}
        , green{0}
        , blue{0}
        {
        }
    };

    ///////////////////////////////////////////////////////////////////////////
    struct Game
    {
        std::vector<Reveal> reveals;
        uint8_t id;
    };

    ///////////////////////////////////////////////////////////////////////////
    std::optional<uint8_t> getNumberFromString(const std::string& string)
    {
        try
        {
            return static_cast<uint8_t>(std::stoi(string));
        }
        catch(const std::exception& e)
        {
            std::cerr << "In string '" << string << "' std::stoi failed with " << e.what() << '\n';
            return std::nullopt;
        }
        
    }
}

///////////////////////////////////////////////////////////////////////////
int main()
{
    const std::filesystem::path inputFile{std::filesystem::current_path() / "puzzleInput.txt"};
    std::ifstream calibrationData{inputFile};
    if (!calibrationData.is_open())
    {
        std::cerr << "Could not open file at " << inputFile << ".\n";
        return -2;
    }

    std::vector<Game> games;

    std::string currentLine;
    while(std::getline(calibrationData, currentLine))
    {
        const auto numberBegin = currentLine.find(" ");
        const auto numberEnd = currentLine.find(":");
        std::string id = currentLine.substr(numberBegin + 1, numberEnd - numberBegin - 1);

        Game game;
        const auto hasId = getNumberFromString(id);
        if (!hasId.has_value())
            continue;
        game.id = hasId.value();

        bool isValidGame = true;

        std::string::iterator nextRevealIndex;
        for (auto revealIndex = currentLine.begin() + numberEnd; revealIndex != currentLine.end(); revealIndex = nextRevealIndex)
        {
            nextRevealIndex = std::find(revealIndex + 1, currentLine.end(), ';');
            Reveal currentReveal{};

            std::string::iterator nextNumberIndex;
            for (auto numberIndex = revealIndex; numberIndex != nextRevealIndex; numberIndex = nextNumberIndex)
            {
                nextNumberIndex = std::find(numberIndex + 1, nextRevealIndex, ',');

                const auto numberIndexBegin = numberIndex + 2;
                const auto numberIndexEnd = std::find(numberIndexBegin, nextNumberIndex, ' ');
                const std::string currentStringNumber{numberIndexBegin, numberIndexEnd};
                const auto currentNumber = getNumberFromString(currentStringNumber);

                const std::string_view colorName{numberIndexEnd + 1, nextNumberIndex};

                if ("blue" == colorName)
                {
                    if (currentNumber.value() > 14)
                    {
                        isValidGame = false;
                        break;
                    }
                    currentReveal.blue = currentNumber.value();
                }
                else if ("green" == colorName)
                {
                    if (currentNumber.value() > 13)
                    {
                        isValidGame = false;
                        break;
                    }
                    currentReveal.green = currentNumber.value();
                }
                else if ("red" == colorName)
                {
                    if (currentNumber.value() > 12)
                    {
                        isValidGame = false;
                        break;
                    }
                    currentReveal.red = currentNumber.value();
                }
            }

            game.reveals.emplace_back(std::move(currentReveal));
        }

        if (isValidGame)
            games.emplace_back(std::move(game));
    }

    std::cout << "There where " << games.size() << " valid games.\n";
    const auto sum = std::accumulate(games.begin(), games.end(), 0, [](const auto& acc, const auto& value) {return acc + value.id; });
    std::cout << "The sum of there IDs is " << sum << "\n";

    return 0;
}