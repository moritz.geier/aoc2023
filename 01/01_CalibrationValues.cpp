// std
#include <algorithm>
#include <cctype>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>

int main()
{
    const std::filesystem::path inputFile{std::filesystem::current_path() / "calibrationData.txt"};
    std::ifstream calibrationData{inputFile};
    if (!calibrationData.is_open())
    {
        std::cout << "Could not open file at " << inputFile << ".\n";
        return -2;
    }

    long sum = 0;
    std::string currentLine;
    while(std::getline(calibrationData, currentLine))
    {
        const auto isDigit = [](const auto& character) { return std::isdigit(character) != 0; };
        const auto firstDigit = std::find_if(currentLine.begin(), currentLine.end(), isDigit);
        if (firstDigit == currentLine.end())
            continue;
        const auto lastDigit = std::find_if(currentLine.rbegin(), currentLine.rend(), isDigit);

        std::string number{*firstDigit};
        number += *lastDigit;
        sum += std::stol(number);
    }

    std::cout << "Your calibration number is " << sum << ".\n";

    return 0;
}