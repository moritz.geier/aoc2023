// std
#include <algorithm>
#include <cctype>
#include <concepts>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <optional>
#include <string>
#include <unordered_map>

///////////////////////////////////////////////////////////////////////////
template<class Iterator>
concept StringIterator = std::disjunction_v<std::is_same<Iterator, std::string::iterator>, std::is_same<Iterator, std::string::reverse_iterator>, std::is_same<Iterator, std::string::const_iterator>, std::is_same<Iterator, std::string::const_reverse_iterator>>;

///////////////////////////////////////////////////////////////////////////
namespace
{
    ///////////////////////////////////////////////////////////////////////////
    template<StringIterator Iterator>
    std::optional<unsigned int> findDigit(const Iterator& begin, const Iterator& end)
    {
        const std::unordered_map<std::string, long> digits{
            {"zero", 0},
            {"one", 1},
            {"two", 2},
            {"three", 3},
            {"four", 4},
            {"five", 5},
            {"six", 6},
            {"seven", 7},
            {"eight", 8},
            {"nine", 9}
        };

        for (auto currentPosition = begin; currentPosition != end; currentPosition++)
        {
            std::string_view currentSubstring;
            if constexpr (std::is_same<Iterator, std::string::iterator>().value || std::is_same<Iterator, std::string::const_iterator>().value)
                currentSubstring = {currentPosition, end};
            else
                currentSubstring = {currentPosition.base(), begin.base()};

            for (const auto& [stringName, value] : digits)
            {
                if (currentSubstring.starts_with(stringName))
                    return value;
            }

            if (std::isdigit(*currentPosition) != 0)
                return std::stoi(std::string{*currentPosition});
        }

        return std::nullopt;
    }
}

///////////////////////////////////////////////////////////////////////////
int main()
{
    const std::filesystem::path inputFile{std::filesystem::current_path() / "calibrationData.txt"};
    std::ifstream calibrationData{inputFile};
    if (!calibrationData.is_open())
    {
        std::cout << "Could not open file at " << inputFile << ".\n";
        return -2;
    }

    long sum = 0;
    std::string currentLine;
    while(std::getline(calibrationData, currentLine))
    {
        const auto firstDigit = findDigit(currentLine.begin(), currentLine.end());
        if (!firstDigit.has_value())
            continue;
        const auto lastDigit = findDigit(currentLine.rbegin(), currentLine.rend());

        sum += firstDigit.value() * 10 + lastDigit.value();
    }

    std::cout << "Your calibration number is " << sum << ".\n";

    return 0;
}