// std
#include <algorithm>
#include <cctype>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <numeric>
#include <ranges>
#include <string>
#include <vector>

///////////////////////////////////////////////////////////////////////////
namespace
{
    ///////////////////////////////////////////////////////////////////////////
    std::optional<uint32_t> getNumberFromString(const std::string& string)
    {
        try
        {
            auto view = string
                | std::views::drop_while(::isspace);
            return static_cast<uint32_t>(std::stoi(std::string{std::string_view{view}}));
        }
        catch(const std::exception& e)
        {
            return std::nullopt;
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    uint32_t calculateTotalCards(const std::vector<uint32_t>::iterator& current, const std::vector<uint32_t>::iterator& end)
    {
        if (current == end)
            return 0;

        uint32_t result = 1;

        const std::vector<uint32_t>::iterator nextEnd = current + *current + 1;
        for (auto next = current + 1; next != end && next != nextEnd; next++)
        {
            result += calculateTotalCards(next, end);
        }

        return result;
    }
}

///////////////////////////////////////////////////////////////////////////
int main()
{
    const std::filesystem::path inputFilePath{std::filesystem::current_path() / "ScratchCards.txt"};
    std::ifstream inputFile{inputFilePath};
    if (!inputFile.is_open())
    {
        std::cerr << "Could not open file at " << inputFilePath << ".\n";
        return -2;
    }

    std::string currentLine{std::istreambuf_iterator<char>(inputFile), std::istreambuf_iterator<char>()};
    std::vector<uint32_t> wins;

    auto view = currentLine
        | std::views::split('\n')
        | std::views::transform([](const auto& view) { return std::string{std::string_view{view}}; })
        | std::views::transform([](const auto& string) { const auto position = string.find(':') + 2; return string.substr(position < string.length() ? position : 0); });

    for (const auto& v : view)
    {
        auto subview = v
            | std::views::split('|')
            | std::views::transform([](const auto& view) { return std::string{std::string_view{view}}; });
        for (auto s = subview.begin(); s != subview.end(); s++)
        {
            constexpr auto getValues = std::views::split(' ')
                | std::views::transform([](const auto& view) { return std::string_view{view}; })
                | std::views::transform([](const auto& view) { return getNumberFromString(std::string{view}); })
                | std::views::filter([](const auto& element) { return element.has_value(); })
                | std::views::transform([](const auto element) { return element.value(); });

            auto winnerNumbers = *s | getValues;

            s++;
            if (s == subview.end())
                break;
            auto ourNumbers = *s| getValues;
            const auto numberOfWins = std::ranges::fold_left(ourNumbers, 0, [&winnerNumbers](const auto& acc, const auto& number) { return std::ranges::find(winnerNumbers, number) != winnerNumbers.end() ? acc + 1 : acc; });
            wins.push_back(numberOfWins);
        }
    }

    uint32_t result = 0;
    for (auto card = wins.begin(); card != wins.end(); card++)
        result += calculateTotalCards(card, wins.end());
    std::cout << "You have " << result << " cards.\n";

    return 0;
}