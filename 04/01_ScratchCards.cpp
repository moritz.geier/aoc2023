// std
#include <algorithm>
#include <cctype>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <numeric>
#include <ranges>
#include <string>

///////////////////////////////////////////////////////////////////////////
namespace
{
    ///////////////////////////////////////////////////////////////////////////
    std::optional<uint32_t> getNumberFromString(const std::string& string)
    {
        try
        {
            auto view = string
                | std::views::drop_while(::isspace);
            return static_cast<uint32_t>(std::stoi(std::string{std::string_view{view}}));
        }
        catch(const std::exception& e)
        {
            // std::cerr << "In string '" << string << "' std::stoi failed with " << e.what() << '\n';
            return std::nullopt;
        }
    }
}

///////////////////////////////////////////////////////////////////////////
int main()
{
    const std::filesystem::path inputFilePath{std::filesystem::current_path() / "ScratchCards.txt"};
    std::ifstream inputFile{inputFilePath};
    if (!inputFile.is_open())
    {
        std::cerr << "Could not open file at " << inputFilePath << ".\n";
        return -2;
    }

    uint32_t points;
    std::string currentLine{std::istreambuf_iterator<char>(inputFile), std::istreambuf_iterator<char>()};

    auto view = currentLine
        | std::views::split('\n')
        | std::views::transform([](const auto& view) { return std::string{std::string_view{view}}; })
        | std::views::transform([](const auto& string) { const auto position = string.find(':') + 2; return string.substr(position < string.length() ? position : 0); });

    for (const auto& v : view)
    {
        auto subview = v
            | std::views::split('|')
            | std::views::transform([](const auto& view) { return std::string{std::string_view{view}}; });
        for (auto s = subview.begin(); s != subview.end(); s++)
        {
            constexpr auto getValues = std::views::split(' ')
                | std::views::transform([](const auto& view) { return std::string_view{view}; })
                | std::views::transform([](const auto& view) { return getNumberFromString(std::string{view}); })
                | std::views::filter([](const auto& element) { return element.has_value(); })
                | std::views::transform([](const auto element) { return element.value(); });

            auto winnerNumbers = *s | getValues;

            s++;
            if (s == subview.end())
                break;
            auto ourWinningNumbers = *s
                | getValues
                | std::views::filter([&winnerNumbers](const auto& number) { return std::ranges::find(winnerNumbers, number) != winnerNumbers.end(); });

            if (!std::ranges::empty(ourWinningNumbers))
            {
                const auto shiftBy = std::ranges::fold_left(ourWinningNumbers, 0, [](const auto& acc, const auto&) { return acc + 1; }) - 1;
                points += (1 << shiftBy);
            }
        }
    }

    std::cout << "The scratchcards are worth " << points << " points.\n";

    return 0;
}