// std
#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <numeric>
#include <optional>
#include <ranges>
#include <regex>
#include <string>

///////////////////////////////////////////////////////////////////////////
namespace
{
    ///////////////////////////////////////////////////////////////////////////
    std::optional<uint32_t> getNumberFromString(const std::string& string)
    {
        try
        {
            return static_cast<uint32_t>(std::stoi(string));
        }
        catch(const std::exception& e)
        {
            std::cerr << "In string '" << string << "' std::stoi failed with " << e.what() << '\n';
            return std::nullopt;
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    bool isGear(char character)
    {
        switch (character)
        {
        case '*':
            return true;
        default:
            return false;
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    uint32_t getAllGears(std::multimap<uint32_t, uint32_t>& gears, uint32_t lineOffset, const std::string& lastLine, const std::string& currentLine, const std::string& nextLine)
    {
        std::regex regex{"\\d+"};
        const auto matchesBegin = std::sregex_iterator(currentLine.begin(), currentLine.end(), regex);
        const auto matchesEnd = std::sregex_iterator();

        const auto nextLineoffset = lineOffset + currentLine.length();
        const auto lastLineOffset = lineOffset - lastLine.length();
        std::regex findGear{"\\*"};

        for (auto matches = matchesBegin; matches != matchesEnd; matches++)
        {
            const auto& match = *matches;
            const char beforeMatch = *(match.prefix().second - 1);
            const char afterMatch = *match.suffix().first;

            const size_t beginIndex = match.position() - 1;
            const size_t endIndex = beginIndex + match.length() + 2;

            std::string_view overMatch;
            if (lastLine.length() > endIndex)
                overMatch = {lastLine.begin() + beginIndex, lastLine.begin() + endIndex};
            else if (lastLine.length() > beginIndex)
                overMatch = {lastLine.begin() + beginIndex, lastLine.end()};

            std::string_view underMatch;
            if (nextLine.length() > endIndex)
                underMatch = {nextLine.begin() + beginIndex, nextLine.begin() + endIndex};
            else if (nextLine.length() > beginIndex)
                underMatch = {nextLine.begin() + beginIndex, nextLine.end()};

            const auto number = getNumberFromString(match[0]).value_or(1);

            if (isGear(beforeMatch))
                gears.emplace(lineOffset + beginIndex, number);

            if (isGear(afterMatch))
                gears.emplace(lineOffset + endIndex - 1, number);

            for (auto overMatchGear = std::sregex_iterator(std::string::const_iterator{overMatch.begin()}, std::string::const_iterator{overMatch.end()}, findGear); overMatchGear != std::sregex_iterator(); overMatchGear++)
            {
                const auto index = std::distance(lastLine.begin(), overMatchGear->operator[](0).first);
                gears.emplace(lastLineOffset + index, number);
            }

            for (auto underMatchGear = std::sregex_iterator(std::string::const_iterator{underMatch.begin()}, std::string::const_iterator{underMatch.end()}, findGear); underMatchGear != std::sregex_iterator(); underMatchGear++)
            {
                const auto index = std::distance(nextLine.begin(), underMatchGear->operator[](0).first);
                gears.emplace(nextLineoffset + index, number);
            }
        }

        return nextLineoffset;
    }
}

///////////////////////////////////////////////////////////////////////////
int main()
{
    const std::filesystem::path inputFilePath{std::filesystem::current_path() / "EngineSchematics.txt"};
    std::ifstream inputFile{inputFilePath};
    if (!inputFile.is_open())
    {
        std::cerr << "Could not open file at " << inputFilePath << ".\n";
        return -2;
    }

    std::multimap<uint32_t, uint32_t> gears;
    uint32_t lineNumber = 0;

    std::string lastLine;
    std::getline(inputFile, lastLine);
    std::string currentLine;
    std::getline(inputFile, currentLine);

    lineNumber = getAllGears(gears, lineNumber, "", lastLine, currentLine);

    std::string nextLine;
    while(std::getline(inputFile, nextLine))
    {
        lineNumber = getAllGears(gears, lineNumber, lastLine, currentLine, nextLine);
        lastLine = currentLine;
        currentLine = nextLine;
    }

    getAllGears(gears, lineNumber, lastLine, currentLine, "");

    auto gearRatios = gears
        | std::views::chunk_by([](const auto& pairOne, const auto& pairTwo) { return pairOne.first == pairTwo.first; })
        | std::views::filter([](const auto& chunk) { return std::ranges::fold_left(chunk, 0, [](const auto& acc, const auto& /*value*/) { return acc + 1; }) == 2; })
        | std::views::transform([](const auto& elements) { return std::ranges::fold_left(elements, 1, [](const auto& acc, const auto& value) { return acc * value.second; }); });
    uint32_t sumOfGearRatios = std::ranges::fold_left(gearRatios, 0, [](const auto& acc, const auto& value) { return acc + value; });

    std::cout << "The sum of all gear ratios is " << sumOfGearRatios << ".\n";

    return 0;
}