// std
#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <numeric>
#include <optional>
#include <regex>
#include <string>
#include <vector>

///////////////////////////////////////////////////////////////////////////
namespace
{
    ///////////////////////////////////////////////////////////////////////////
    std::optional<uint32_t> getNumberFromString(const std::string& string)
    {
        try
        {
            return static_cast<uint32_t>(std::stoi(string));
        }
        catch(const std::exception& e)
        {
            std::cerr << "In string '" << string << "' std::stoi failed with " << e.what() << '\n';
            return std::nullopt;
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    bool isSymbol(char character)
    {
        switch (character)
        {
        case '*':
        case '+':
        case '-':
        case '/':
        case '@':
        case '$':
        case '=':
        case '%':
        case ';':
        case ':':
        case ',':
        case '<':
        case '>':
        case '\\':
        case '?':
        case '^':
        case '#':
        case '!':
        case '&':
        case '|':
        case '~':
            return true;
        default:
            return false;
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    void getNumbersFromSchematic(std::vector<uint32_t>& numbers, const std::string& lastLine, const std::string& currentLine, const std::string& nextLine)
    {
        std::regex regex{"\\d+"};
        const auto matchesBegin = std::sregex_iterator(currentLine.begin(), currentLine.end(), regex);
        const auto matchesEnd = std::sregex_iterator();

        for (auto matches = matchesBegin; matches != matchesEnd; matches++)
        {
            const auto& match = *matches;
            const char beforeMatch = *(match.prefix().second - 1);
            const char afterMatch = *match.suffix().first;

            const size_t beginIndex = match.position() - 1;
            const size_t endIndex = beginIndex + match.length() + 2;

            std::string_view overMatch;
            if (lastLine.length() > endIndex)
                overMatch = {lastLine.begin() + beginIndex, lastLine.begin() + endIndex};
            else if (lastLine.length() > beginIndex)
                overMatch = {lastLine.begin() + beginIndex, lastLine.end()};

            std::string_view underMatch;
            if (nextLine.length() > endIndex)
                underMatch = {nextLine.begin() + beginIndex, nextLine.begin() + endIndex};
            else if (nextLine.length() > beginIndex)
                underMatch = {nextLine.begin() + beginIndex, nextLine.end()};

            if (isSymbol(beforeMatch) || isSymbol(afterMatch) || std::ranges::any_of(overMatch, isSymbol) || std::ranges::any_of(underMatch, isSymbol))
                numbers.push_back(getNumberFromString(match[0]).value_or(0));
        }
    }
}

///////////////////////////////////////////////////////////////////////////
int main()
{
    const std::filesystem::path inputFilePath{std::filesystem::current_path() / "EngineSchematics.txt"};
    std::ifstream inputFile{inputFilePath};
    if (!inputFile.is_open())
    {
        std::cerr << "Could not open file at " << inputFilePath << ".\n";
        return -2;
    }

    std::vector<uint32_t> validNumbers;
    validNumbers.reserve(200);

    std::string lastLine;
    std::getline(inputFile, lastLine);
    std::string currentLine;
    std::getline(inputFile, currentLine);

    getNumbersFromSchematic(validNumbers, "", lastLine, currentLine);

    std::string nextLine;
    while(std::getline(inputFile, nextLine))
    {
        getNumbersFromSchematic(validNumbers, lastLine, currentLine, nextLine);
        lastLine = currentLine;
        currentLine = nextLine;
    }

    getNumbersFromSchematic(validNumbers, lastLine, currentLine, "");

    std::cout << "The part number is " << std::accumulate(validNumbers.begin(), validNumbers.end(), 0) << ".\n";

    return 0;
}