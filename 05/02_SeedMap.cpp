// omp
#include <omp.h>

// std
#include <algorithm>
#include <cctype>
#include <charconv>
#include <filesystem>
#include <format>
#include <fstream>
#include <iostream>
#include <limits>
#include <numeric>
#include <ranges>
#include <string>
#include <vector>

///////////////////////////////////////////////////////////////////////////
namespace
{
    ///////////////////////////////////////////////////////////////////////////
    struct SeedMap
    {
        uint32_t inputBegin;
        uint32_t outputBegin;
        uint32_t length;
    };

    ///////////////////////////////////////////////////////////////////////////
    std::optional<uint32_t> getNumberFromString(const std::string_view& string)
    {
        uint32_t number;
        const auto result = std::from_chars(string.data(), string.data() + string.size(), number);

        if (result.ec == std::errc::invalid_argument || result.ec == std::errc::result_out_of_range)
            return std::nullopt;
        return number;
    }

    inline bool isBetween(uint32_t value, uint32_t lower, uint32_t upper)
    {
        return lower <= value && value <= upper;
    }

    ///////////////////////////////////////////////////////////////////////////
    std::vector<SeedMap> mapRanges(const std::vector<SeedMap>& input, const std::vector<SeedMap>& maps)
    {
        std::vector<SeedMap> newMap;
        newMap.reserve(input.size());

        for (const auto& range : input)
        {
            const auto rangeOutputEnd = range.outputBegin + range.length;
            for (const auto& map : maps)
            {
                const auto mapInputEnd = map.inputBegin + map.length;
                if (map.inputBegin < range.outputBegin && isBetween(mapInputEnd, range.outputBegin, rangeOutputEnd))
                {
                    // range:     |-------------|
                    // map  : |-----|
                    const uint32_t newInputBegin = range.inputBegin;
                    const uint32_t newOutputBegin = map.outputBegin + range.outputBegin - map.inputBegin;
                    const uint32_t newLength = mapInputEnd - range.outputBegin;
                    newMap.emplace_back(newInputBegin, newOutputBegin, newLength);
                }
                else if (isBetween(map.inputBegin, range.outputBegin, rangeOutputEnd) && rangeOutputEnd <= mapInputEnd)
                {
                    // range:     |-------------|
                    // map  :               |-----|
                    const uint32_t newInputBegin = range.inputBegin + map.inputBegin - range.outputBegin;
                    const uint32_t newOutputBegin = map.outputBegin;
                    const uint32_t newLength = rangeOutputEnd - map.inputBegin;
                    newMap.emplace_back(newInputBegin, newOutputBegin, newLength);
                }
                else if (map.inputBegin < range.outputBegin && rangeOutputEnd <= mapInputEnd)
                {
                    // range:     |-------------|
                    // map  :        |-----|
                    const uint32_t newInputBegin = range.inputBegin;
                    const uint32_t newOutputBegin = map.outputBegin + range.outputBegin - map.inputBegin;
                    const uint32_t newLength = range.length;
                    newMap.emplace_back(newInputBegin, newOutputBegin, newLength);
                }
                else if (map.inputBegin >= range.outputBegin && rangeOutputEnd >= mapInputEnd)
                {
                    // range:     |-------------|
                    // map  : |---------------------|
                    const uint32_t newInputBegin = range.inputBegin + map.inputBegin - range.outputBegin;
                    const uint32_t newOutputBegin = map.outputBegin;
                    const uint32_t newLength = map.length;
                    newMap.emplace_back(newInputBegin, newOutputBegin, newLength);
                }
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////
int main()
{
    const std::filesystem::path inputFilePath{std::filesystem::current_path() / "SeedMap.txt"};
    std::ifstream inputFile{inputFilePath};
    if (!inputFile.is_open())
    {
        std::cerr << "Could not open file at " << inputFilePath << ".\n";
        return -2;
    }

    std::string seedLine;
    std::getline(inputFile, seedLine);

    constexpr auto stringToIntList = std::views::split(' ')
        | std::views::transform([](const auto& view) { return getNumberFromString(std::string_view{view}); })
        | std::views::filter([](const auto& number) { return number.has_value(); })
        | std::views::transform([](const auto& number) { return number.value(); });

    auto seedView = seedLine
        | stringToIntList
        | std::views::chunk(2);

    std::string mapLines{std::istreambuf_iterator<char>(inputFile), std::istreambuf_iterator<char>()};

    auto mapView = mapLines
        | std::views::split('\n')
        | std::views::transform([](const auto& view) { return std::string_view{view}; })
        | std::views::filter([](const auto& string) { return std::isspace(string[0]) || std::isdigit(string[0]); })
        | std::views::chunk_by([](const auto& stringOne, const auto& stringTwo) { return !stringOne.empty() && !stringTwo.empty(); })
        | std::views::filter([](const auto& view) { return 1 < std::ranges::distance(view); });
    (void) mapView;

    const uint32_t totalSeeds = std::ranges::fold_left(seedView, 0, [](const auto& acc, const auto& view) { const auto iterator = ++view.begin(); return acc + (iterator != view.end() ? *iterator : 0); });
    const float progressIndicator = totalSeeds / 100.0f;

    std::cout << "[";
    std::optional<uint32_t> closestSeed{};
    uint32_t progressCounter = 0;
    for (const auto& seedRange : seedView)
    {
        auto iterator = seedRange.begin();
        const uint32_t rangeBegin = *iterator;
        iterator++;
        if (iterator == seedRange.end())
            continue;
        const uint32_t rangeLength = *iterator;

        std::vector<std::pair<std::optional<uint32_t>, uint32_t>> seeds;
        seeds.reserve(rangeLength);
        for (uint32_t seed = rangeBegin; seed < rangeBegin + rangeLength; seed++)
            seeds.emplace_back(seed, seed);

        #pragma omp parallel for shared(progressCounter)
        for (auto& seed : seeds)
        {
            for (const auto& view : mapView)
            {
                for (const auto& subview : view)
                {
                    auto currentMapView = subview | stringToIntList;
                    if (3 != std::ranges::distance(currentMapView))
                        continue;

                    auto iterator = currentMapView.begin();
                    const uint32_t transfromedValueBegin = *iterator;
                    iterator++;
                    const uint32_t originalValueBegin = *iterator;
                    iterator++;
                    const uint32_t rangeLength = *iterator;

                    const auto& toTransform = seed.first;
                    if (toTransform.has_value() && originalValueBegin <= toTransform && originalValueBegin + rangeLength > toTransform)
                    {
                        seed.second = toTransform.value() - originalValueBegin + transfromedValueBegin;
                        seed.first = std::nullopt;
                    }
                }

                seed.first = seed.second;
            }

            progressCounter++;
            if (1.0f < progressCounter / progressIndicator)
            {
                progressCounter = 0;
                std::cout << '-';
            }
        }

        const auto result = std::ranges::min_element(seeds);
        if (result != seeds.end())
            closestSeed = std::min(closestSeed.value_or(std::numeric_limits<uint32_t>().max()), result->second);
    }

    std::cout << "]\n";

    if (closestSeed.has_value())
        std::cout << "The nearest seed is at " << closestSeed.value() << ".\n";
    else
        std::cout << "Didn't find any seeds";

    return 0;
}