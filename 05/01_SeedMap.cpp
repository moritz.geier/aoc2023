// std
#include <algorithm>
#include <cctype>
#include <charconv>
#include <filesystem>
#include <format>
#include <fstream>
#include <iostream>
#include <numeric>
#include <ranges>
#include <string>
#include <vector>

///////////////////////////////////////////////////////////////////////////
namespace
{
    ///////////////////////////////////////////////////////////////////////////
    std::optional<uint32_t> getNumberFromString(const std::string_view& string)
    {
        uint32_t number;
        const auto result = std::from_chars(string.data(), string.data() + string.size(), number);

        if (result.ec == std::errc::invalid_argument || result.ec == std::errc::result_out_of_range)
            return std::nullopt;
        return number;
    }
}

///////////////////////////////////////////////////////////////////////////
int main()
{
    const std::filesystem::path inputFilePath{std::filesystem::current_path() / "SeedMap.txt"};
    std::ifstream inputFile{inputFilePath};
    if (!inputFile.is_open())
    {
        std::cerr << "Could not open file at " << inputFilePath << ".\n";
        return -2;
    }

    std::string seedLine;
    std::getline(inputFile, seedLine);

    constexpr auto stringToIntList = std::views::split(' ')
        | std::views::transform([](const auto& view) { return getNumberFromString(std::string_view{view}); })
        | std::views::filter([](const auto& number) { return number.has_value(); })
        | std::views::transform([](const auto& number) { return number.value(); });

    auto seedView = seedLine | stringToIntList;

    std::string mapLines{std::istreambuf_iterator<char>(inputFile), std::istreambuf_iterator<char>()};

    auto mapView = mapLines
        | std::views::split('\n')
        | std::views::transform([](const auto& view) { return std::string_view{view}; })
        | std::views::filter([](const auto& string) { return std::isspace(string[0]) || std::isdigit(string[0]); })
        | std::views::chunk_by([](const auto& stringOne, const auto& stringTwo) { return !stringOne.empty() && !stringTwo.empty(); })
        | std::views::filter([](const auto& view) { return 1 < std::ranges::distance(view); });

    std::vector<std::pair<std::optional<uint32_t>, uint32_t>> seeds;
    seeds.resize(std::ranges::distance(seedView));
    std::ranges::transform(seedView, seeds.begin(), [](const auto& element) { return std::pair<std::optional<uint32_t>, uint32_t>{element, element}; });

    for (const auto& view : mapView)
    {
        for (const auto& subview : view)
        {
            auto currentMapView = subview | stringToIntList;
            if (3 != std::ranges::distance(currentMapView))
                continue;

            auto iterator = currentMapView.begin();
            const uint32_t transfromedValueBegin = *iterator;
            iterator++;
            const uint32_t originalValueBegin = *iterator;
            iterator++;
            const uint32_t rangeLength = *iterator;

            for (auto& [toTransform, transformed] : seeds)
            {
                if (toTransform.has_value() && originalValueBegin <= toTransform && originalValueBegin + rangeLength > toTransform)
                {
                    transformed = toTransform.value() - originalValueBegin + transfromedValueBegin;
                    toTransform = std::nullopt;
                }
            }
        }

        std::ranges::transform(seeds, seeds.begin(), [](const auto& element) { return std::pair<std::optional<uint32_t>, uint32_t>{element.second, element.second}; });
    }

    const auto result = std::ranges::min_element(seeds);
    if (result != seeds.end())
        std::cout << "The nearest seed is at " << result->second << ".\n";
    else
        std::cout << "Didn't find any seeds";

    return 0;
}